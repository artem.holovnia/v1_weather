import os

from django.conf import settings, Settings
from django.contrib import messages
from django.contrib.auth import login as dj_login, logout as dj_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.db.models.query_utils import Q
from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls.base import reverse_lazy, reverse

import cities
from cities.decorators import empty_subscriptions, subscriptions_limit
from cities.models import City, Subscription


@login_required(login_url=reverse_lazy('cities:login'))
def cities(request):
    page = request.GET.get('page') or 1
    query = request.GET.get('query')
    cities = City.objects.filter(Q(name__icontains=query.strip()) | Q(country__icontains=query.strip())) if query else City.objects.all()
    if request.user.is_authenticated:
        cities = cities.exclude(id__in=Subscription.objects.filter(user_id=request.user.id).values_list('city_id', flat=True))
    cities = cities.order_by('country', 'name')
    paginator = Paginator(cities, settings.CITIES_PER_PAGE)
    page_queryset = cities.filter(id__in=paginator.page(page).object_list.values_list('id', flat=True))
    countries = list(set(page_queryset.values_list('country', flat=True)))
    return render(request, 'cities.html', {'cities':page_queryset, 'countries':countries, 'paginator':paginator.page(page)})

@login_required(login_url=reverse_lazy('cities:login'))
@subscriptions_limit
def subscribe(request, id):
    Subscription.objects.update_or_create(user=request.user, city=get_object_or_404(City, id=id))
    return redirect(reverse('cities:subscriptions-list'))

@login_required(login_url=reverse_lazy('cities:login'))
def unsubscribe(request, id):
    get_object_or_404(Subscription, user=request.user, city__id=id).delete()
    return redirect(reverse('cities:subscriptions-list'))

@login_required(login_url=reverse_lazy('cities:login'))
@empty_subscriptions
def subscriptions(request):
    cities = City.objects.filter(id__in=Subscription.objects.select_related('city').filter(user=request.user).values_list('city__id', flat=True))
    return render(request, 'subscriptions.html', {'cities':cities})

def login(request):
    if request.method == 'POST':
        try:
            email, password = request.POST.get('email'), request.POST.get('password')
            user = User.objects.get(email=email)
            assert user.check_password(password)
            dj_login(request, user)
            return redirect(reverse('cities:subscriptions-list'))
        except (User.DoesNotExist, AssertionError):
            messages.add_message(request, messages.WARNING, 'Login failed')
    return render(request, 'login.html', {})

@login_required(login_url=reverse_lazy('cities:login'))
def logout(request):
    dj_logout(request)
    return redirect(reverse('cities:cities-list'))

def readme(request):
    return HttpResponse(open(os.path.join(settings.BASE_DIR, 'README.md'), 'rb'))
