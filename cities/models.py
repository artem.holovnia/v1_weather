from django.contrib.auth.models import User
from django.db import models


class City(models.Model):
    name = models.CharField('City name', max_length=100)
    country = models.CharField('Country name', max_length=100)
    temperature = models.SmallIntegerField('Temperature', null=True)
    
    class Meta:
        unique_together = ('name', 'country')

class Subscription(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    
    class Meta:
        unique_together = ('user', 'city')