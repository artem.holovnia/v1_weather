from django.core.management.base import BaseCommand

from cities.models import City
import pandas as pd


class Command(BaseCommand):
    
    def add_arguments(self, parser):
        parser.add_argument('--country', nargs = 1, type = str, help = 'Country name for dataset filtering')
        parser.add_argument('--city', nargs = 1, type = str, help = 'City name for dataset filtering')
        parser.add_argument('--limit', nargs = 1, type = str, help = 'Number of datasets rows')
        parser.add_argument('--offset', nargs = 1, type = str, help = 'Number of row dataset start from')
    
    def handle(self, *args, **options):
        country, city = options.get('country'), options.get('city')
        limit, offset = options.get('limit'), options.get('offset')
        country = country[0] if country else country
        city = city[0] if city else city
        limit = int(limit[0]) if limit else limit
        offset = int(offset[0]) if offset else offset
        
        df = pd.read_csv('dataset.csv', sep=',')
        if city:
            df.query(f'city=="{city.capitalize()}"', inplace=True)
        if country:
            df.query(f'country=="{country.capitalize()}"', inplace=True)
        df.loc[:, ['city', 'country']]
        df = df.groupby(['city', 'country'], as_index=False).count()
        City.objects.bulk_create([City(name=i.city, country=i.country) for i in df.itertuples()])
